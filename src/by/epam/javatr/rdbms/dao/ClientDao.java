package by.epam.javatr.rdbms.dao;

import java.util.Set;

import by.epam.javatr.rdbms.bean.Client;
import by.epam.javatr.rdbms.bean.Good;
import by.epam.javatr.rdbms.bean.GoodsSet;
import by.epam.javatr.rdbms.dao.exception.DaoException;

/***
 * при получении клиента из базы не получаем автоматически его товары - их по 
 * отдельному запросу getGoods
 * 
 * @author Sergii_Kotov
 *
 */
public interface ClientDao {
	/**
	 * добавление клиента в хранилище
	 * @param c клиент
	 * @return
	 */
	public boolean addClient (Client c) throws DaoException ;
	/**
	 * достать из хранилища клиента(ов) по имени
	 * @param n имя
	 * @return множество клиентов
	 */
	public Set<Client> getClientByName (String n) throws DaoException ;
	public Client getClientById (int id) throws DaoException ;
	public void delClient (Client c) throws DaoException ;
	public Set<Client> getAllClients () throws DaoException ;
	
	/**
	 * выдать товар клиенту
	 * @param g товар
	 * @param с клиент
	 * @return успешно ли?
	 */
	public boolean giveGoodToClient(Good g, Client c) throws DaoException ;
	/**
	 * оформить возвращение товара от клиента
	 * @param g возвращаемый товар
	 * @param с клиент
	 * @return успешно ли?
	 */
	public boolean recievGoodFromClient(Good g, Client c) throws DaoException ;
	/**
	 * получить из хранилища список товаров клиента
	 * @param c клиент
	 * @return	множество товаров
	 */
	public GoodsSet getGoods(Client c) throws DaoException ;
}
