package by.epam.javatr.rdbms.dao.implementation;

import java.sql.Connection;
import java.util.HashSet;
import java.util.Set;

import by.epam.javatr.rdbms.bean.Client;
import by.epam.javatr.rdbms.bean.Good;
import by.epam.javatr.rdbms.bean.GoodsSet;
import by.epam.javatr.rdbms.dao.ClientDao;
import by.epam.javatr.rdbms.dao.exception.DaoException;

/***
 * ЗАПОЛНИТЬ!!!!!!!!!!!!!!!!!!!!!!
 * @author Sergii_Kotov
 *
 */
public class SQLClient implements ClientDao{
	Connection con;
	/***
	 * в фабрике установлена связь с базой - берем ее оттуда
	 * @param c 
	 */
	public SQLClient (Connection c){
		con=c;
	}

	@Override
	public boolean addClient (Client c) throws DaoException {
		return true;
	}
	@Override
	public Set<Client> getClientByName (String n)  throws DaoException {
		HashSet<Client> hs= new HashSet<Client>();
		hs.add(new Client());
		hs.add(new Client());
		return hs;
	}
	
	@Override
	public Client getClientById (int id) throws DaoException {
		return new Client();
	}
	@Override
	public void delClient (Client c) throws DaoException {
		
	}
	@Override
	public Set<Client> getAllClients () throws DaoException {
		HashSet<Client> hs= new HashSet<Client>();
		hs.add(new Client());
		hs.add(new Client());
		return hs;
	}
	
	@Override
	public boolean giveGoodToClient(Good g, Client c) throws DaoException {
		return false;
	}
	@Override
	public boolean recievGoodFromClient(Good g, Client c) throws DaoException {
		return false;
	}
	@Override
	public GoodsSet getGoods(Client c) throws DaoException {
		return null;
	}
	
}
