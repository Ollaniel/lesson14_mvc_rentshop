package by.epam.javatr.rdbms.dao;

import by.epam.javatr.rdbms.bean.Good;
import by.epam.javatr.rdbms.bean.GoodsSet;
import by.epam.javatr.rdbms.dao.exception.DaoException;

public interface GoodDao {
	/***
	 * добавление товара в хранилище
	 * @param g товар
	 * @return успешно ли (может уже был)
	 */
	public boolean addGood (Good g) throws DaoException ;
	/**
	 * достать из хранилища товар (товары) по имени
	 * @param n имя - регистронезависимый поиск подстроки
	 * @return набор товаров 
	 */
	public GoodsSet getGoodByName (String n) throws DaoException ;
	/**
	 * только один конкретный товар по PK
	 * @param id код товара
	 * @return найденный товар
	 */
	public Good getGoodById (int id) throws DaoException ;
	/***
	 * удаление товара(ов) 
	 * @param g список товаров
	 */
	public void delGood (GoodsSet g) throws DaoException ;
	/**
	 * вообще все товары в базе
	 * @return
	 */
	public GoodsSet getAllGoods () throws DaoException ;

}
