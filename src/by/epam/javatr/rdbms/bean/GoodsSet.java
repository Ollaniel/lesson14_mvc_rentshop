package by.epam.javatr.rdbms.bean;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class GoodsSet implements Serializable {
	private static final long serialVersionUID = 1L;

	/***
	 * множество товаров на складе это сет. 
	 * Не должно быть повторяющихся товаров. 
	 */
	private Set<Good> stGood;
	
	public GoodsSet() {
		stGood=new HashSet <Good>();
	}

	public GoodsSet(Set <Good> stGood) {
		this.stGood = stGood;
	}

	public boolean add(Good g) {
		return stGood.add(g);
	}
	
	public int size() {
		return stGood.size();
	}
	
	

}
