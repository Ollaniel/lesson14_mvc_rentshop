package by.epam.javatr.rdbms.bean;

import java.io.Serializable;

public class Client implements Serializable{
	private static final long serialVersionUID = 1L;
	private String name;
	private int id;
	private double account;
	private boolean isActive;
	/***
	 * как отобразить связь клиента и книг?
	 * не зачитывать же из базы все книги которые у него на руках   
	 */
	private GoodsSet goods;


public Client() {
	this("empty", 0, 0, null);
}

public Client(String name, int id, double account, GoodsSet goods) {
		this.name = name;
		this.id = id;
		this.account = account;
		this.goods = goods;
		isActive=true;
	}
//------------------------  можно ли в бине заводить методы обработки данных?
public void setGoods(GoodsSet goods) {
	this.goods=goods;
}

public void addGood(Good good) {
	this.goods.add(good);
}


//------------------------  

@Override
public String toString() {
	return "Client " + name + " id=" + id +". "+(isActive?"Active":"Not active") +". Current account=" + account + ". Keeped books: "+((goods==null)?0:goods.size());
}



@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	long temp;
	temp = Double.doubleToLongBits(account);
	result = prime * result + (int) (temp ^ (temp >>> 32));
	result = prime * result + ((goods == null) ? 0 : goods.hashCode());
	result = prime * result + id;
	result = prime * result + (isActive ? 1231 : 1237);
	result = prime * result + ((name == null) ? 0 : name.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Client other = (Client) obj;
	if (Double.doubleToLongBits(account) != Double.doubleToLongBits(other.account))
		return false;
	if (goods == null) {
		if (other.goods != null)
			return false;
	} else if (!goods.equals(other.goods))
		return false;
	if (id != other.id)
		return false;
	if (isActive != other.isActive)
		return false;
	if (name == null) {
		if (other.name != null)
			return false;
	} else if (!name.equals(other.name))
		return false;
	return true;
}
public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public double getAccount() {
	return account;
}

public void setAccount(double account) {
	this.account = account;
}

public void setActive(boolean a) {
	this.isActive = a;
}

public boolean getActive() {
	return isActive;
}

}
