package by.epam.javatr.rdbms.service;

import by.epam.javatr.rdbms.bean.Good;
import by.epam.javatr.rdbms.bean.GoodsSet;
import by.epam.javatr.rdbms.service.exception.ServiceException;

public interface GoodService {
	public boolean addGood (Good g) throws ServiceException ;
	public GoodsSet getGoodByName (String n) throws ServiceException ;
	public Good getGoodById (int id) throws ServiceException ;
	public void delGood (GoodsSet g) throws ServiceException ;
	public GoodsSet getAllGoods () throws ServiceException ;
}
