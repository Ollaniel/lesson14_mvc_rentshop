package by.epam.javatr.rdbms.service.implementation;

import by.epam.javatr.rdbms.bean.Good;
import by.epam.javatr.rdbms.bean.GoodsSet;
import by.epam.javatr.rdbms.dao.GoodDao;
import by.epam.javatr.rdbms.dao.exception.DaoException;
import by.epam.javatr.rdbms.dao.factory.DaoFactory;
import by.epam.javatr.rdbms.service.GoodService;
import by.epam.javatr.rdbms.service.exception.ServiceException;

public class GoodServiceImpl implements GoodService {

	@Override
	public boolean addGood(Good g) throws ServiceException {
		// проверка параметров 
		if ((g==null)||(g.getInventID()<=0)) {
			throw new ServiceException("Wrong good parameters!");
		}
		try {
			DaoFactory daoObjectFactory = DaoFactory.getInstance();
			GoodDao GdDao = daoObjectFactory.getGoodDao();
			return GdDao.addGood(g);
		} catch (DaoException e){
			throw new ServiceException(e);
		}
	}

	@Override
	public GoodsSet getGoodByName(String n) throws ServiceException {
		try {
			DaoFactory daoObjectFactory = DaoFactory.getInstance();
			GoodDao GdDao = daoObjectFactory.getGoodDao();
			return GdDao.getGoodByName(n);
		} catch (DaoException e){
			throw new ServiceException(e);
		}
	}

	@Override
	public Good getGoodById(int id) throws ServiceException {
		// проверка параметров 
		if (id>0) {
			throw new ServiceException("Wrong good parameters!");
		}
		try {
			DaoFactory daoObjectFactory = DaoFactory.getInstance();
			GoodDao GdDao = daoObjectFactory.getGoodDao();
			return GdDao.getGoodById(id);
		} catch (DaoException e){
			throw new ServiceException(e);
		}
	}

	@Override
	public void delGood(GoodsSet g) throws ServiceException {
		// проверка параметров 
		if (g==null) {
			throw new ServiceException("Wrong good parameters!");
		}
		try {
			DaoFactory daoObjectFactory = DaoFactory.getInstance();
			GoodDao GdDao = daoObjectFactory.getGoodDao();
			GdDao.delGood(g);
		} catch (DaoException e){
			throw new ServiceException(e);
		}
	}

	@Override
	public GoodsSet getAllGoods() throws ServiceException {
		try {
			DaoFactory daoObjectFactory = DaoFactory.getInstance();
			GoodDao GdDao = daoObjectFactory.getGoodDao();
			return GdDao.getAllGoods();
		} catch (DaoException e){
			throw new ServiceException(e);
		}
	}

}
