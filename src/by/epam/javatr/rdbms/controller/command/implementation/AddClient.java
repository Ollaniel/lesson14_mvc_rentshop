package by.epam.javatr.rdbms.controller.command.implementation;

import by.epam.javatr.rdbms.bean.Client;
import by.epam.javatr.rdbms.controller.command.Command;
import by.epam.javatr.rdbms.service.ClientService;
import by.epam.javatr.rdbms.service.exception.ServiceException;
import by.epam.javatr.rdbms.service.factory.ServiceFactory;

public class AddClient implements Command{
	Client c;
	@Override
	public String execute(String request) {
		//"AddClient sName dAccount";
		String response = "ok";
		String [] params=request.split(" ");
		try {
			c = new Client (params[1],0,Double.valueOf(params[2]),null);
		} catch (ArrayIndexOutOfBoundsException | NumberFormatException e) {
			response = "wrong request! we need (string, string, double) here.";
		}
		
		if (response.equals("ok")) {
			// get parameters from request and initialize the variables login and password
			ServiceFactory serviceFactory = ServiceFactory.getInstance();
			ClientService clientService = serviceFactory.getCLientService();
			try {
				if (clientService.addClient(c)) {
					response = "Client has been added.";
				} else {
					response = "Client has NOt been added.";
				}
			} catch (ServiceException e) {
				// write log
				response = "Error duiring adding the client";
			}
		}
		return response;
	}
}
