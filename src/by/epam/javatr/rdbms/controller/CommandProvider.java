package by.epam.javatr.rdbms.controller;

import java.util.HashMap;
import java.util.Map;

import by.epam.javatr.rdbms.controller.command.Command;
import by.epam.javatr.rdbms.controller.command.implementation.AddClient;
import by.epam.javatr.rdbms.controller.command.implementation.GetClientByName;

public class CommandProvider {
	private final static CommandProvider instance = new CommandProvider();  
	enum Task{ WRONG_RESULT, ADDCLIENT,GETCLIENTBYNAME, GETCLIENTBYID, DELCLIENT, GETALLCLIENTS, GIVEGOODTOCLIENT,
		RECIEVGOODFROMCLIENT,GETGOODS,ADDGOOD,GETGOODBYNAME,GETGOODBYID,DELGOOD,GETALLGOODS}
	private final Map <Task,Command> tasks = new HashMap<Task,Command>();
	
	private CommandProvider () {
			tasks.put(Task.GETCLIENTBYNAME,new GetClientByName());
			tasks.put(Task.ADDCLIENT,new AddClient());
			//tasks.put(Task.ADDGOOD,new AddGood());
	}
	
	public static CommandProvider getInstance () {
		return instance;
	}
	
	public Command getCmd (String request){
		try {
			String command= request.substring(0, request.indexOf(" "));
			return tasks.get(Task.valueOf(command));
		} catch (Exception e ){//(NullPointerException | IllegalArgumentException e) {
		// вот-вот, конеыно надо ловить конкретные исключения
			return null; //tasks.get(Task.WRONG_RESULT);
		}
	}
}
